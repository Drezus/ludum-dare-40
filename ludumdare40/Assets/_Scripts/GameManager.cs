﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    private void Awake()
    {
        //DontDestroyOnLoad(gameObject);
    }

    public Camera MainCam;

    [Header("Gameplay Variables")]
    public int _coins = 0;
    public int Coins
    {
        get
        {
            return _coins;
        }
        set
        {
            _coins = value;
            HUDManager.Instance.SetCoins(_coins);
        }
    }

    public static int _rooms = 0;
    public static int Room
    {
        get
        {
            return _rooms;
        }
        set
        {
            _rooms = value;
            HUDManager.Instance.SetRoom(_rooms);
        }
    }

    public static int _tier = 0;
    public static int Tier
    {
        get
        {
            return _tier;
        }
        set
        {
            _tier = value;
            HUDManager.Instance.SetTier(_tier);
        }
    }

    [Header("Coin Reward Variables")]
    public int TierCoinLimit = 10;
    public float TierPrizeMultiplier = 1.2f;

    [Header("Doors")]
    public DoorManager DoorManager;

    [Header("Tutorial")]
    public DialogueSequence TutorialSequence;

    [Header("Sounds")]
    public AudioSource MainCamAudioSource;
    public AudioClip Drumroll;
    public AudioClip Cymbal;

    [Header("Input")]
    public bool FreezePlayer = false;
    public bool FreezeEnemy = false;
    public bool FreezeAll = false;

    [Header("WIP/Testing")]
    public DialogueSequence MacacoSequence;
    public DialogueSequence TreasureSequence;
    public Text BigScreenText;

    public void OnEnable()
    {
        FreezeAll = false;

        //if (Tier == 0)
        //{
        //        Tier++;
        //}

        Tier++;

        DoorManager.CreateDoors(Tier + 1);

        if (Room == 0)
        {
            BigScreenText.text = "TUTORIAL";
            BigScreenText.rectTransform.sizeDelta = new Vector3(0, 0, 0);
            BigScreenText.rectTransform.DOSizeDelta(new Vector3(1, 1, 1), 0.4f).SetEase(Ease.OutBack);

            DialogueManager.Instance.ShowDialogueSequence(TutorialSequence);
        }
        else if (Room == 1)
        {
            BigScreenText.text = "MONKEY ROOM";

            MainCamAudioSource.PlayOneShot(Cymbal);
            DialogueManager.Instance.ShowDialogueSequence(MacacoSequence);
        }
        else if (Room == 2)
        {
            BigScreenText.text = "TREASURE ROOM";

            MainCamAudioSource.PlayOneShot(Cymbal);
            DialogueManager.Instance.ShowDialogueSequence(TreasureSequence);
        }
        else
        {
            BigScreenText.text = "ETC";
            MainCamAudioSource.PlayOneShot(Cymbal);
        }

        Room++;

        HUDManager.Instance.UpdateHUD(Room, Coins, Tier);
    }

    public void ZoomToDoor(Vector3 pos)
    {
        FreezeAll = true;
        MainCamAudioSource.PlayOneShot(Drumroll);
        MainCam.transform.DOMove(pos, 5);
        HUDManager.Instance.DoorFade(() =>
        {

            SceneManager.LoadSceneAsync(0);
        });
    }
}
