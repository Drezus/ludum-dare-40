﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public Rigidbody ParticleFragmentPrefab;
    public int FragmentCount;
    public float Force = 600;
    
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            for(var i = 0; i < FragmentCount; i++)
            {
                var fragment = Instantiate(ParticleFragmentPrefab, transform.position, transform.rotation);
                var x = Random.Range(-1f, 1f);
                var z = Random.Range(-1f, 1f);
                var direction = new Vector3(x, 1, z);
                fragment.AddForce(direction.normalized * Force);
            }
        }
    }
}
