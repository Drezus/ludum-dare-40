﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionFragment : MonoBehaviour
{
    public float TimeToDestroy;
    public Rigidbody body;
    public ParticleSystem pSystem;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(TimeToDestroy);
        body.isKinematic = true;
        body.velocity = Vector3.zero;
        yield return new WaitForSeconds(1);
        pSystem.Stop(true);
        Destroy(gameObject);
    }

    public void AddForce(Vector3 force)
    {
        body.AddForce(force);
    }
}
