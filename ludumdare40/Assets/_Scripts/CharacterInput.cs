﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInput : MonoBehaviour
{
    public Character Character;

    private void Awake()
    {
        if (Character == null)
            Debug.LogException(new System.Exception("Missing character reference."));
    }

    private void Update()
    {
        if (GameManager.Instance.FreezePlayer || GameManager.Instance.FreezeAll)
            return;

        var vertical = 0;
        if (Input.GetKey(KeyCode.W))
            vertical = 1;
        else if (Input.GetKey(KeyCode.S))
            vertical = -1;

        var horizontal = 0;
        if (Input.GetKey(KeyCode.D))
            horizontal = 1;
        else if (Input.GetKey(KeyCode.A))
            horizontal = -1;

        var direction = new Vector3(horizontal, 0, vertical);
        if (direction.magnitude > 0)
            Character.Move(direction);
        else
            Character.StopMovement();

        if (Input.GetKeyDown(KeyCode.Space))
            Character.Jump(direction);
    }
}
