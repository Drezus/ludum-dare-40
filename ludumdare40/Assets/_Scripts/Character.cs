﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Character : MonoBehaviour
{
    public CharacterState CurrentState;

    [Header("Components")]
    public Rigidbody Rigidbody;
    public Transform Visual;

    [Header("Attributes")]
    public int MaxHealth;
    public int CurrentHealth;
    public int Power;

    [Header("Movement")]
    public float MoveSpeed;
    public float TurnDuration;
    public float JumpForce;
    public bool Grounded;
    public float AirMinDistance;

    private Tween _turnTween;

    [Header("Culling Layers")]
    public LayerMask GroundLayer;

    public Vector3 Position { get { return transform.position; } }

    private void FixedUpdate()
    {
        if (GameManager.Instance.FreezeAll)
            return;

        if (!Grounded)
        {
            if (Rigidbody.velocity.y < 0)
            {
                var ray = new Ray(transform.position, -Vector3.up);
                if (Physics.Raycast(ray, AirMinDistance, GroundLayer))
                {
                    Rigidbody.velocity = Vector3.zero;
                    Grounded = true;
                }
            }

            if(Mathf.Abs(Rigidbody.velocity.x) > 6)
            {
                var sign = Mathf.Sign(Rigidbody.velocity.x);
                Rigidbody.velocity = new Vector3(6 * sign, Rigidbody.velocity.y, Rigidbody.velocity.z);
            }

            if(Mathf.Abs(Rigidbody.velocity.z) > 6)
            {
                var sign = Mathf.Sign(Rigidbody.velocity.z);
                Rigidbody.velocity = new Vector3(Rigidbody.velocity.x, Rigidbody.velocity.y, 6 * sign);
            }
            
        }
    }

    #region Movement
    public void Move(Vector3 direction)
    {
        if (Grounded)
        {
            Turn(direction);
            Rigidbody.velocity = Vector3.zero;
            Rigidbody.MovePosition(transform.position + (direction.normalized * MoveSpeed * Time.deltaTime));
        }
        else
        {
            Turn(direction);
            Rigidbody.AddForce(direction * MoveSpeed);
        }
    }

    public void Jump(Vector3 direction)
    {
        if (Grounded)
        {
            var dir = new Vector3(direction.normalized.x * 0.7f, 1f, direction.normalized.z *0.7f);
            Rigidbody.AddForce(dir * JumpForce);
            Grounded = false;
        }
    }

    public void Turn(Vector3 direction)
    {
        var angle = (Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg);
        _turnTween = Visual.DORotate(new Vector3(0, angle, 0), TurnDuration);
    }

    public void StopMovement()
    {
        if (_turnTween != null)
            _turnTween.Kill();
    }
    #endregion

    #region Battle
    public void TakeDamage(Character attacker, int damage)
    {
        CurrentHealth = Mathf.Max(0, CurrentHealth - damage);
        if (CurrentHealth == 0)
            SetState(CharacterState.Dead);
    }

    public void DoDamage(Character target)
    {
        target.TakeDamage(this, Power);
    }
    #endregion

    protected void SetState(CharacterState newState)
    {
        CurrentState = newState;
    }
}
