﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mugshot : MonoBehaviour
{
    public Image EyebrowImg;
    public Image MouthImg;

    public Sprite[] Eyebrows;
    public Sprite[] Mouths;

    public void SetMugshot(MugshotEyebrows eye, MugshotMouths mo)
    {
        EyebrowImg.sprite = Eyebrows[(int)eye];
        MouthImg.sprite = Mouths[(int)mo];
    }
}

public enum MugshotEyebrows
{
    Regular,
    Confused,
    Sad,
    Angry
}

public enum MugshotMouths
{
    Regular,
    Shouting,
    Smiling,
    Sad
}
