﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class DialogueManager : Singleton<DialogueManager>
{
    public delegate void PageCallback();

    public Image MugshotImage;
    public Mugshot MugshotScript;
    public Text DialogueText;
    public Transform DialogueBubble;

    public GameObject NextText;

    public AudioSource VoiceSource;
    public AudioClip[] GenericVoiceLibrary;

    private int _pageIndex = 0;
    private int _maxPages = 0;

    private DialogueSequence _currentSequence;

    bool _visible = false;
    public bool Visible { get { return _visible; } }

    bool _tweening = false;

    public void ShowDialogueSequence(DialogueSequence seq)
    {
        _currentSequence = seq;
        NextText.SetActive(_currentSequence.RequiresPlayerInput);
        GameManager.Instance.FreezeAll = _currentSequence.RequiresPlayerInput;

        if (_currentSequence.Pages.Length > 0)
        {
            _pageIndex = 0;
            _maxPages = _currentSequence.Pages.Length;

            MugshotImage.rectTransform.position = new Vector2(MugshotImage.rectTransform.position.x, -400);
            DialogueBubble.localScale = Vector3.zero;

            if (!_visible)
            {
                MugshotEnteringAnim(() =>
                {
                    ShowPage(_currentSequence.Pages[_pageIndex]);
                });
            }
            else
            {
                ShowPage(_currentSequence.Pages[_pageIndex]);
            }
        }
        else
        {
            Debug.LogError("Dialogue sequence without pages");
        }
    }

    void MugshotEnteringAnim(PageCallback pageCall)
    {
        _tweening = true;
        MugshotImage.rectTransform.DOAnchorPosY(-200, 0.3f).SetEase(Ease.OutBack).OnComplete(() =>
        {
            _visible = true;

            if (pageCall != null)
                pageCall();
        });
    }

    void ShowPage(DialoguePage p)
    {
        PlayPageVoice(p);
        DialogueText.text = p.Dialogue;
        DialogueText.fontStyle = p.Bold ? FontStyle.Bold : FontStyle.Normal;
        DialogueText.alignment = p.Align == BalloonTextAlignment.Center ? TextAnchor.MiddleCenter : TextAnchor.MiddleLeft;

        MugshotScript.SetMugshot(p.Eyebrows, p.Mouth);
        MugshotImage.rectTransform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
        MugshotImage.rectTransform.DOScale(new Vector3(1, 1, 1), 0.2f).SetEase(Ease.OutBack);

        Ease thisEase = Ease.OutExpo;
        if (p.Easing == BalloonEasing.Back) thisEase = Ease.OutBack;
        if (p.Easing == BalloonEasing.Elastic) thisEase = Ease.OutElastic;

        _tweening = true; 
        if(p.ShakeStrength > 0)
        {
            DialogueBubble.DOShakePosition(0.8f, p.ShakeStrength, 50, 90, false, false);
            MugshotImage.rectTransform.DOShakePosition(0.8f, p.ShakeStrength/2, 50, 90, false, false);
        }

        DialogueBubble.DOScale(new Vector3(p.BaloonSize, p.BaloonSize, p.BaloonSize), 0.4f).SetEase(thisEase).OnComplete(() =>
        {
            _tweening = false;

            if(!_currentSequence.RequiresPlayerInput)
            {
                StartCoroutine(AutoNextPage(p.Duration));
            }
        });
    }

    void PlayPageVoice(DialoguePage p)
    {
        ///Plays a random voice from the page's settings unless there's no specific voice to play there, in which case plays any voice from the game.
        if(p.PossibleSounds.Length > 0)
        {
            VoiceSource.PlayOneShot(p.PossibleSounds[UnityEngine.Random.Range(0, p.PossibleSounds.Length - 1)]);
        }
        else
        {
            VoiceSource.PlayOneShot(GenericVoiceLibrary[UnityEngine.Random.Range(0, GenericVoiceLibrary.Length - 1)]);
        }
    }

    private void Update()
    {
        if(_currentSequence != null)
        {
            if (Input.GetButtonDown("Fire1") && !_tweening && _currentSequence.RequiresPlayerInput == true)
            {
                NextPage();
            }
        }
    }

    void NextPage()
    {
        if (!_currentSequence)
            return;

        DialogueBubble.localScale = Vector3.zero;
        _pageIndex++;
        if(_pageIndex > _currentSequence.Pages.Length - 1)
        {
            EndDialogue();
        }
        else
        {
            ShowPage(_currentSequence.Pages[_pageIndex]);
        }
    }

    IEnumerator AutoNextPage(float duration)
    {
        yield return new WaitForSeconds(duration);
        NextPage();
    }

    void EndDialogue()
    {
        DialogueBubble.localScale = Vector3.zero;
        _pageIndex = 0;
        _currentSequence = null;

        MugshotImage.rectTransform.DOAnchorPosY(-400, 0.3f).SetEase(Ease.InBack).OnComplete(() =>
        {
            MugshotImage.rectTransform.position = new Vector2(MugshotImage.rectTransform.position.x, -400);
            _visible = false;
            GameManager.Instance.FreezeAll = false;
        });
    }
}
