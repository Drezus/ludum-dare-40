﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Dialogue", menuName = "LudumDare40/New Dialogue")]
public class DialogueSequence : ScriptableObject
{
    [Tooltip("Determines if text bubbles appear automatically as defined by their duration time " +
    "and leaves player input free, or if they must be skipped by pressing a button, locking player input.")]
    public bool RequiresPlayerInput = true;
    [SerializeField]
    public DialoguePage[] Pages;
}
