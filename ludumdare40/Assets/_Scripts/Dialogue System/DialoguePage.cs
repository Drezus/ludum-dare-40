﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialoguePage
{
    [Header("Dialogue Content")]
    [SerializeField]
    public string Dialogue;
    [SerializeField]
    public MugshotEyebrows Eyebrows;
    [SerializeField]
    public MugshotMouths Mouth;
    [SerializeField]
    [Tooltip("If certain songs are preferred for this bubble, list their DialogueManager's SoundLibrary array index here.")]
    public AudioClip[] PossibleSounds;

    [Header("Visual Effects")]
    [SerializeField]
    public BalloonTextAlignment Align = BalloonTextAlignment.Left;
    [SerializeField]
    public BalloonEasing Easing = BalloonEasing.Regular;
    [SerializeField]
    public bool Bold = false;
    [SerializeField]
    public float ShakeStrength = 0;
    [SerializeField]
    public float BaloonSize = 1;

    [SerializeField]
    [Tooltip("Duration is only used in cases the dialogue advances automatically without player input.")]
    public float Duration = 0;
}

public enum BalloonTextAlignment
{
    Left,
    Center
}

public enum BalloonEasing
{
    Regular,
    Back,
    Elastic
}
