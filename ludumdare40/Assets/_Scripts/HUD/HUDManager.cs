﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : Singleton<HUDManager>
{
    public delegate void DoorCallback();

    public Text _roomText;
    public Text _tierText;
    public Text _coinText;
    public Image _healthBar;
    public Image _doorFade;

    public void OnEnable()
    {
        _doorFade.color = new Color(0, 0, 0, 0);
    }

    public void SetRoom(int num)
    {
        _roomText.text = "Room #" + num;
    }

    public void SetTier(int num)
    {
        _tierText.text = "TIER " + num;
    }

    public void SetCoins(int num)
    {
        _coinText.text = "Coins: " + num;
    }

    public void SetHealthBar(float percentage)
    {
        _healthBar.fillAmount = percentage;
    }

    public void DoorFade(DoorCallback callback)
    {
        _doorFade.DOColor(new Color(0, 0, 0, 1), 3).OnComplete(()=>
        {
            if (callback != null)
                callback();
        });
    }

    public void UpdateHUD(int room, int coin, int tier)
    {
        SetRoom(room);
        SetCoins(coin);
        SetTier(tier);
    }
}
