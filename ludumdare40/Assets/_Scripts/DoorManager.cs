﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : Singleton<DoorManager>
{
    public Transform Center;
    public float DoorDistance;
    public Door DoorPrefab;
    public List<Door> Doors;

    public void CreateDoors (int doorCount)
    {
        float initial;

        if (doorCount % 2 == 0)
        {
            initial = -(DoorDistance * doorCount * 0.5f) + (DoorDistance * 0.5f);
        }
        else
        {
            initial = -DoorDistance * Mathf.Floor(doorCount * 0.5f);
        }

        for (var i = 0; i < doorCount; i++)
        {
            var door = Instantiate(DoorPrefab, transform);
            Doors.Add(door);
            

            var xOffset = DoorDistance * i;
            door.transform.position = new Vector3(initial + xOffset, Center.position.y, Center.position.z);
        }
    }
}
