﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAI : MonoBehaviour
{
    public Character Character;
    private Character Target;

    private void Awake()
    {
        if (Character == null)
            Debug.LogException(new System.Exception("Missing character reference."));
    }

    // Use this for initialization
    void Start()
    {
        var targetGameObject = GameObject.FindGameObjectWithTag("Player");
        Target = targetGameObject.GetComponent<Character>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.FreezeEnemy || GameManager.Instance.FreezeAll)
            return;

        if (Target != null)
        {
            Character.Move(Target.Position - Character.Position);
            if (Vector3.Distance(Character.Position, Target.Position) > 4)
                Character.Jump(Target.Position - Character.Position);
        }
    }
}
