﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Chest : MonoBehaviour
{
    public Rigidbody body;
    public ExplosionFragment fragment;
    public ParticleSystem pSystem;
    public Light chestLight;
    public Transform ChestTop;
    public Transform[] explosionPoints;

    [Header("Chest Info")]
    public int Gold;

    [Header("Animation")]
    public bool Opened;
    public float OpenedAngleX;
    public float ClosedAngleX;
    public float LightIntensity;

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Ground"))
        {
            Camera.main.transform.DOShakePosition(0.4f, 0.2f, 100, 90, true);
            Camera.main.transform.DOShakeRotation(0.4f, 5, 100, 90, true);
            for(var i = 0; i < explosionPoints.Length; i++)
            {
                var frag = Instantiate(fragment, explosionPoints[i].position, explosionPoints[i].rotation);
                frag.AddForce(explosionPoints[i].up * 800);
            }

            body.velocity = Vector3.zero;
            body.isKinematic = true;
        }

        if(collision.collider.CompareTag("Player"))
        {
            OpenChest();
        }
    }

    public void OpenChest()
    {
        if (Opened)
            return;

        Opened = true;
        DOTween.To(() => ClosedAngleX, x => ChestTop.localEulerAngles = new Vector3(x, 0, 0), OpenedAngleX, 0.3f).OnComplete(pSystem.Play);
        DOTween.To(() => chestLight.intensity, intensity => chestLight.intensity = intensity, LightIntensity, 0.6f);
    }
}
